import os
import re
import subprocess

shell.prefix(". /mnt/lustre/Tools/Software/Python/versions/miniconda3-latest/etc/profile.d/conda.sh; ")

# load configfiles
configfile: "Configs/config.yaml"

include: "Scripts/SnakemakeCore/SnakemakeRules/snakemake_helpers.rule"
include: "Scripts/ProjectSpecificRules/helper_functions.rule"

include: "Scripts/SnakemakeCore/SnakemakeRules/salmon.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/picard.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/fastqc.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/trim_galore.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/star.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/hisat2.rule"
include: "Scripts/ProjectSpecificRules/tximport_no_counts_from_abundance.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/tximport.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/multiqc.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/run_deseq2.rule"
include: "Scripts/SnakemakeCore/SnakemakeRules/annotate_deseq2.rule"
include: "Scripts/ProjectSpecificRules/prepare_phenotypes.rule"
include: "Scripts/ProjectSpecificRules/atac_seq_analysis_charotte.rule"
include: "Scripts/ProjectSpecificRules/plot_pca.rule"
include: "Scripts/ProjectSpecificRules/run_deseq2_old_batches.rule"
include: "Scripts/ProjectSpecificRules/run_deseq2_diabetes.rule"
include: "Scripts/ProjectSpecificRules/run_deseq2_diabetes_no_age_filtering.rule"
include: "Scripts/ProjectSpecificRules/run_deseq2_linear_model.rule"
include: "Scripts/ProjectSpecificRules/run_deseq2_insulin_secretion.rule"
include: "Scripts/ProjectSpecificRules/run_deseq2_glucagon_secretion.rule"
include: "Scripts/ProjectSpecificRules/run_phenotype_nont2d_lrt.rule"
include: "Scripts/ProjectSpecificRules/run_bmi_lrt_tina_117_samples.rule"
include: "Scripts/ProjectSpecificRules/run_lrt_si.rule"




rule all:
    input: 
           "RDataWorkspaces/tximport_no_counts_from_abundance.RData",
           multiqc_at = "QC/MultiQC/multiqc_after_adapter_trimming_and_filtering_samples.html",
           multiqc_bt = "QC/MultiQC/multiqc_before_adapter_trimming.html",

           ##atac = expand("Output/Models/DESeq2_tximport_atac/{model}.txt", model=get_deseq2_models()),
           plot = "Plots/pca_all_batches.png",
           plot2 = "Plots/pca_hiseq.png",
           plot3 = "Plots/pca_nextseq.png",

           ##deseq2 models
	   ## models = expand("Output/Models/DESeq2_tximport/{model}.txt", model=get_deseq2_models()),
           models_hiseq = expand("Output/Models/DESeq2_tximport_hiseq_diabetes/{model}.txt", model=get_deseq2_models()),
           models_nextseq = expand("Output/Models/DESeq2_tximport_nextseq_diabetes/{model}.txt", model=get_deseq2_models()),
           models = expand("Output/Models/DESeq2_tximport/{model}.txt", model=get_deseq2_models()),
           deseq2_diabetes = expand("Output/Models/DESeq2_diabetes/{model}.txt", model=get_deseq2_models()),
           deseq2_diabets_no_age_filtering = expand("Output/Models/DESeq2_diabetes_no_age_filtering/{model}.txt", model=get_deseq2_models()),

           ins_sec = "Output/Models/DESeq2_lm_age_bmi_is/1mM_glucose_insulin_secretion.txt",
           ins_sec_16_7 = "Output/Models/DESeq2_lm_age_bmi_is/16_7mM_glucose_insulin_secretion.txt",
           gluc_sec = "Output/Models/DESeq2_lm_age_bmi_gluc/1mM_glucose_glucagon.txt",
           glucagon_secretion = "Output/Models/DESeq2_lm_age_bmi_gluc/16_7mM_glucose_glucagon.txt",
           
           ## linear model expr ~ age + bmi + purity + days_cultured + sex
           lm_hba1c = "Output/Models/DESeq2_lm_age_bmi_hba1c/deseq2_hba1c.txt",
           lm_dic = "Output/Models/DESeq2_lm_age_bmi_hba1c/days_cultured.txt",
           lm_purity = "Output/Models/DESeq2_lm_age_bmi_hba1c/purity.txt",
           lm_bmi = "Output/Models/DESeq2_lm_age_bmi_hba1c/bmi.txt",
           lm_age = "Output/Models/DESeq2_lm_age_bmi_hba1c/age.txt",
           sex = "Output/Models/DESeq2_lm_age_bmi_hba1c/sex.txt",
           
           bmi_lrt = "Output/Models/DESeq2_nondiabetics_lrt/bmi.txt",
           hba1c_lrt = "Output/Models/DESeq2_nondiabetics_lrt/hba1c.txt",
           age_lrt = "Output/Models/DESeq2_nondiabetics_lrt/age.txt",
           sex_lrt = "Output/Models/DESeq2_nondiabetics_lrt/sex.txt",
           purity_lrt = "Output/Models/DESeq2_nondiabetics_lrt/purity.txt",
           dic_lrt = "Output/Models/DESeq2_nondiabetics_lrt/dic.txt",
           ## batch_lrt = "RDataWorkspaces/Models/DESeq2_nondiabetics_lrt/batch.RData",
           si_lrt = "Output/Models/DESeq2_nondiabetics_lrt/stimulatory_index.txt",

           ## export stuff for Tina
           bmi_tina = "Output/Subprojects/Models/Tina_bmi_117_samples/bmi_lrt.txt",

