library("DESeq2")
library("BiocParallel")
register(MulticoreParam(10))


options(echo=TRUE)
args <- commandArgs(trailingOnly = TRUE)

# counts_file <- "Output/feature_counts.txt"
# phenotypes_rdata <- "RDataWorkspaces/phenotypes.RData"
# model <- as.formula("~ Batch + Sympt_Asympt")
# output_file <- "Output/Models/DESeq2/sympt_vs_asympt.txt"

tximport_rdata <- args[1]
phenotypes_rdata <- args[2]
model <- as.formula(args[3])
output_file <- args[4]
contrasts <- args[5:length(args)]


##read phenotypes
phenotypes <- readRDS(phenotypes_rdata)
phenotypes$batch <- as.factor(phenotypes$batch)

## read counts
tximport <- readRDS(tximport_rdata)
stopifnot(all(colnames(tximport$counts) == phenotypes$sample_id))

## produce deseq2 object
dds <- DESeqDataSetFromTximport(tximport, phenotypes, model)

##check if rownames(phenotypes) equal to colnames of counts
dds <- DESeq(dds, parallel=TRUE)
res <- results(dds, contrast = contrasts)
res <- res[ !(is.na(res$padj)), ]
res <- res[ order(res$pvalue), ]

save.image("test.RData")

res <- data.frame(gencode_id = rownames(res), res)
write.table(res, output_file, sep = "\t", quote = F, row.names = F)
