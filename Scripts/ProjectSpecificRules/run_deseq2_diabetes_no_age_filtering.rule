rule annotate_deseq2_diabetes_no_age_filtering:
    input:
        script = "Scripts/SnakemakeCore/R/annotate_deseq2.R",
        rdata = "RDataWorkspaces/Models/DESeq2_diabetes_no_age_filtering/{model}.RData",
        gtf_file = "Annotations/gencode.gtf",
        phenotypes = "RDataWorkspaces/phenotypes_prepared_for_diabetes_model_no_age_filtering.RData",

    output: txt = "Output/Models/DESeq2_diabetes_no_age_filtering/{model}.txt"

    params: contrasts = lambda wildcards : config["models"][wildcards.model]["contrasts"],

    conda: "Configs/environment.yaml"
    shell: "Rscript {input.script} {input.rdata} {input.gtf_file} {output.txt} FALSE {input.phenotypes} {params.contrasts}"

    
rule deseq2_on_diabetes_dataset_no_age_filtering:
    input:
           tximport = "RDataWorkspaces/tximport_prepared_for_diabetes_model_no_age_filtering.RData",
           phenotypes = "RDataWorkspaces/phenotypes_prepared_for_diabetes_model_no_age_filtering.RData",
           script = "Scripts/SnakemakeCore/R/deseq2_tximport.R"
    params:
           ## model parameters
           contrasts = lambda wildcards : config["models"][wildcards.model]["contrasts"],
           model = lambda wildcards : config["models"][wildcards.model]["model"]
    output:
           "RDataWorkspaces/Models/DESeq2_diabetes_no_age_filtering/{model}.RData"
    conda: "Configs/environment.yaml"           
    shell:
           "Rscript {input.script} {input.tximport} {input.phenotypes} '{params.model}' {output} {params.contrasts}"


rule filter_dataset_for_diabetes_model_no_age_filtering:
    input: rdata = "RDataWorkspaces/tximport_salmon.RData",
           script = "Scripts/SnakemakeCore/R/filter_dataset.R",
           phenotypes = "RDataWorkspaces/phenotypes.RData"
    output: dataset = "RDataWorkspaces/tximport_prepared_for_diabetes_model_no_age_filtering.RData",
            phenotypes = "RDataWorkspaces/phenotypes_prepared_for_diabetes_model_no_age_filtering.RData"
    conda: "Configs/environment.yaml"            
    params: nondiagnosed_hba1c = nondiagnosed_hba1c("Annotations/phenotypes.txt"),
            gad = gad("Annotations/phenotypes.txt"),
            gestational = gestational("Annotations/phenotypes.txt"),
            days_in_culture = max_days_in_culture("Annotations/phenotypes.txt", 6)
    shell: "Rscript {input.script} {input.rdata} {input.phenotypes} {output.dataset} {output.phenotypes} {params.nondiagnosed_hba1c} {params.gad} {params.gestational} {params.days_in_culture}"
