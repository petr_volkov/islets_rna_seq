rule annotate_deseq2_sex:
    input:
        script = "Scripts/SnakemakeCore/R/annotate_deseq2.R",
        rdata = "RDataWorkspaces/Models/DESeq2_lm_age_bmi_hba1c/deseq2_hba1c.RData",
        gtf_file = "Annotations/gencode.gtf",
        phenotypes = "RDataWorkspaces/phenotypes_nont2d.RData",

    output: txt = "Output/Models/DESeq2_lm_age_bmi_hba1c/sex.txt"

    params: contrasts = ["gender", "Female", "Male"]

    conda: "Configs/environment.yaml"
    shell: "Rscript {input.script} {input.rdata} {input.gtf_file} {output.txt} FALSE {input.phenotypes} {params.contrasts}"


rule annotate_deseq2_lm_days_cultured:
    input:
        script = "Scripts/SnakemakeCore/R/annotate_deseq2_linear.R",
        rdata = "RDataWorkspaces/Models/DESeq2_lm_age_bmi_hba1c/deseq2_hba1c.RData",
        gtf_file = "Annotations/gencode.gtf",
        phenotypes = "RDataWorkspaces/phenotypes_nont2d.RData",

    output: txt = "Output/Models/DESeq2_lm_age_bmi_hba1c/days_cultured.txt"

    params: contrasts = "days_cultured"

    conda: "Configs/environment.yaml"
    shell: "Rscript {input.script} {input.rdata} {input.gtf_file} {output.txt} FALSE {input.phenotypes} {params.contrasts}"

rule annotate_deseq2_lm_purity:
    input:
        script = "Scripts/SnakemakeCore/R/annotate_deseq2_linear.R",
        rdata = "RDataWorkspaces/Models/DESeq2_lm_age_bmi_hba1c/deseq2_hba1c.RData",
        gtf_file = "Annotations/gencode.gtf",
        phenotypes = "RDataWorkspaces/phenotypes_nont2d.RData",

    output: txt = "Output/Models/DESeq2_lm_age_bmi_hba1c/purity.txt"

    params: contrasts = "purity"

    conda: "Configs/environment.yaml"
    shell: "Rscript {input.script} {input.rdata} {input.gtf_file} {output.txt} FALSE {input.phenotypes} {params.contrasts}"

rule annotate_deseq2_lm_bmi:
    input:
        script = "Scripts/SnakemakeCore/R/annotate_deseq2_linear.R",
        rdata = "RDataWorkspaces/Models/DESeq2_lm_age_bmi_hba1c/deseq2_hba1c.RData",
        gtf_file = "Annotations/gencode.gtf",
        phenotypes = "RDataWorkspaces/phenotypes_nont2d.RData",

    output: txt = "Output/Models/DESeq2_lm_age_bmi_hba1c/bmi.txt"

    params: contrasts = "bmi"

    conda: "Configs/environment.yaml"
    shell: "Rscript {input.script} {input.rdata} {input.gtf_file} {output.txt} FALSE {input.phenotypes} {params.contrasts}"

rule annotate_deseq2_lm_age:
    input:
        script = "Scripts/SnakemakeCore/R/annotate_deseq2_linear.R",
        rdata = "RDataWorkspaces/Models/DESeq2_lm_age_bmi_hba1c/deseq2_hba1c.RData",
        gtf_file = "Annotations/gencode.gtf",
        phenotypes = "RDataWorkspaces/phenotypes_nont2d.RData",

    output: txt = "Output/Models/DESeq2_lm_age_bmi_hba1c/age.txt"

    params: contrasts = "age"

    conda: "Configs/environment.yaml"
    shell: "Rscript {input.script} {input.rdata} {input.gtf_file} {output.txt} FALSE {input.phenotypes} {params.contrasts}"

rule annotate_deseq2_lm_hba1c:
    input:
        script = "Scripts/SnakemakeCore/R/annotate_deseq2_linear.R",
        rdata = "RDataWorkspaces/Models/DESeq2_lm_age_bmi_hba1c/{model}.RData",
        gtf_file = "Annotations/gencode.gtf",
        phenotypes = "RDataWorkspaces/phenotypes_nont2d.RData",

    output: txt = "Output/Models/DESeq2_lm_age_bmi_hba1c/{model}.txt"

    params: contrasts = lambda wildcards : config["models"][wildcards.model]["contrasts"],

    conda: "Configs/environment.yaml"
    shell: "Rscript {input.script} {input.rdata} {input.gtf_file} {output.txt} FALSE {input.phenotypes} {params.contrasts}"

rule deseq2_lm_on_nondiabetes_dataset:
    input:
           tximport = "RDataWorkspaces/tximport_nont2d.RData",
           phenotypes = "RDataWorkspaces/phenotypes_nont2d.RData",
           script = "Scripts/SnakemakeCore/R/deseq2_tximport.R"
    params:
           ## model parameters
           contrasts = lambda wildcards : config["models"][wildcards.model]["contrasts"],
           model = lambda wildcards : config["models"][wildcards.model]["model"]
    output:
           "RDataWorkspaces/Models/DESeq2_lm_age_bmi_hba1c/{model}.RData"
    conda: "Configs/environment.yaml"           
    shell:
           "Rscript {input.script} {input.tximport} {input.phenotypes} '{params.model}' {output} {params.contrasts}"

## we decided to filter some samples before running differential expression for type 2 diabetes
rule filter_dataset_for_lm:
    input: rdata = "RDataWorkspaces/tximport_salmon.RData",
           script = "Scripts/SnakemakeCore/R/filter_dataset.R",
           phenotypes = "RDataWorkspaces/phenotypes.RData"
    output: dataset = "RDataWorkspaces/tximport_nont2d.RData",
            phenotypes = "RDataWorkspaces/phenotypes_nont2d.RData"
    conda: "Configs/environment.yaml"            
    params: nondiagnosed_hba1c = nondiagnosed_hba1c("Annotations/phenotypes.txt"),
            gad = gad("Annotations/phenotypes.txt"),
            gestational = gestational("Annotations/phenotypes.txt"),
            days_in_culture = max_days_in_culture("Annotations/phenotypes.txt", 6),
            diabetes = diagnosed_diabetes("Annotations/phenotypes.txt")
    shell: "Rscript {input.script} {input.rdata} {input.phenotypes} {output.dataset} {output.phenotypes} {params.nondiagnosed_hba1c} {params.gad} {params.gestational} {params.days_in_culture} {params.diabetes}"
